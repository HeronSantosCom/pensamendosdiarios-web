<?php

if (!class_exists("commutator", false))
    include path::plugins("insigndigital/commutator.php");

class main extends commutator {

    public function __construct() {
        parent::__construct();

        if ($_SERVER["REDIRECT_URL"] == "/rss.xml")
            return $this->gRss();

        if ($this->sitemap)
            die($this->sSitemap());

        if (!$this->id)
            knife::redirect("?id=" . $this->gSorteio());

        $this->gPensamento();
    }

    private function gPensamento() {
        $db = new mysqlsearch();
        $db->table("pensamento");
        $db->join("autor", array('autor_id', '=', 'id'), "LEFT");
        $db->column("*", false);
    	$db->column("MD5(`t1`.id)", false, "md5");
        $db->match("MD5(`t1`.id)", $this->id);
        $pensamento = $db->go();
        if (isset($pensamento[0])) {
            $this->extract($pensamento[0]);
            $this->titulo = name . " por {$this->autor_nome}";
            $this->url = urlencode("http://" . str_replace("chrome.", "", domain) . "/?id={$this->md5}");
            return true;
        }
        return $this->gSorteio();
    }

    private function gSorteio() {
        if (!isset($_SESSION["id"])) {
            $_SESSION["id"] = $this->gDiario();
        }
        return $_SESSION["id"];
    }

    private function gDiario() {
        $db = new mysqlsearch();
        $db->table("diario");
        $db->column("md5");
        $db->match("data", date("Y-m-d"));
        $diario = $db->go();
        return (isset($diario[0]) ? $diario[0]["md5"] : $this->sDiario());
    }

    private function sDiario() {
        $id = $this->gNovoId();
        $db = new mysqlsave();
        $db->table("diario");
        $db->column("md5", md5($id));
        $db->column("id_pensamento", $id);
        $db->column("data", date("Y-m-d"));
        if ($db->go()) {
            return $id;
        }
        return $this->sDiario();
    }

    private function gNovoId() {
        $db = new mysqlsearch();
        $db->table("novo");
        $db->column("id");
        $novo = $db->go();
        if (isset($novo[0])) {
            return $novo[0]["id"];
        }
        return $this->gNovoId();
    }

    private function gRss() {
	header_remove("expires");
	$this->gSorteio();
        $this->rss_titulo = name;
        $this->rss_descricao = name . " é um vasta coletânea de pensamentos e frases desconhecidas e famosas de filósofos, autores, personalidades e provérbios de diversas magnitude enriquecendo seu conhecimento e seu modo de interagir.";
        $db = new mysqlsearch();
	$db->alterlocale('en_US');
	$db->table("rss");
        $db->column("*");
        $this->lista = $db->go();
    }

    private function sSitemap($sitemap = false, $total = false, $inicial = 0) {
        if (!$inicial and !$total and !$sitemap) {
            if (!class_exists("sitemap", false))
                include path::plugins("insigndigital/sitemap.php");
            $db = new mysqlsearch();
            $db->table("pensamento");
            $db->column("COUNT(id)", false, "total");
            $total = $db->go();
            $total = $total[0]["total"];
            $sitemap = new Sitemap();
        }
        if ($total > 0 and $inicial < $total) {
            $db = new mysqlsearch();
            $db->table("pensamento");
            $db->column("MD5(id)", false, "md5");
            $db->limit($inicial, 1000);
            $pensamentos = $db->go();
            if (is_array($pensamentos)) {
                foreach ($pensamentos as $pensamento) {
                    $sitemap->url("http://" . domain . "/?id={$pensamento["md5"]}", false, "weekly");
                }
            }
            $this->sSitemap($sitemap, $total, ($inicial + 1000));
        }
        if (!$inicial) {
            return $sitemap->go();
        }
    }

}
